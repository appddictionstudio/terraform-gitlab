provider "vault" {
  version = "~> 2.11"
  address = "https://vault.appddiction1.com"
  auth_login {
    path = "/auth/userpass/login/${tostring(var.vault_username)}"
    parameters = {
      password = tostring(var.vault_password)
    }
  }
}

data "vault_generic_secret" "awsInstructorKeys" {
  path = "kv-engine/appddiction1"
}