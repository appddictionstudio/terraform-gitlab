resource "aws_dynamodb_table" "terraform_state_lock" {
  # Chane the dynamo db table. 
  name           = "pipeline-terraform-lock"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"
  billing_mode   = "PAY_PER_REQUEST"
  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "Db Table"
  }
}