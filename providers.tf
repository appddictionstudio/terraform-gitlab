# AWS Configure
provider "aws" {
  version    = "~> 2.67"
  region     = var.region
  access_key = data.vault_generic_secret.awsInstructorKeys.data["dev-sec-ops-instructor-access-id"]
  secret_key = data.vault_generic_secret.awsInstructorKeys.data["dev-sec-ops-instructor-secret-key"]
}