resource "tls_private_key" "host" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_key_pair" "generated_key" {

  key_name   = "terraform-gitlab"
  public_key = tls_private_key.host.public_key_openssh

}

resource "aws_instance" "pipeline_gitlab_server" {
  count = var.pipeline_ec2_count
  key_name                    = aws_key_pair.generated_key.key_name
  ami                         = "ami-02354e95b39ca8dec"
  instance_type               = "t2.medium"
  subnet_id                   = aws_subnet.pipeline_public_us_east_1a.id
  associate_public_ip_address = true
  root_block_device {
    volume_size = "8"
    volume_type = "gp2"
  }
  provisioner "remote-exec" {
      scripts = ["C:\\Users\\travis.vela\\Documents\\DevSecOpsBootcamp\\terraform-gitlab\\modules\\aws\\scripts\\config.sh"]
  connection {
      type = "ssh"
      user = "ec2-user"
      password = ""
      private_key = data.vault_generic_secret.awsInstructorKeys.data["terraform-gitlab-pem"]
      host = self.public_ip
      }
      
  }
    depends_on = [aws_key_pair.generated_key]
  tags = {
    PROJECTTYPE = "pipeline"
    Name        = "pipeline-gitlab-${count.index + 1}"
  }

  vpc_security_group_ids = [
    aws_security_group.pipeline_sg.id
  ]
}

output "ssh-key" {
  value       = aws_key_pair.generated_key
}
