resource "aws_security_group" "pipeline_sg" {
  name        = "pipeline-sg"
  description = "Used for Docker EE Security Group Configuration"
  vpc_id      = aws_vpc.pipeline_vpc.id

  # Access via HTTP to Test Websites
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # SSH from Internet Gateway
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # RDP into the machine
  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "pipeline sg"
    PROJECTTYPE = "pipeline"
  }
}

resource "aws_security_group" "pipeline_elb_sg" {
  name        = "pipeline-elb-sg"
  description = "Used for Docker EE Security Group Configuration"
  vpc_id      = aws_vpc.pipeline_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "pipeline-elb-sg"
    PROJECTTYPE = "pipeline"
  }
}