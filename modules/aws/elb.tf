# VPC ELB
resource "aws_security_group" "pipeline_vpc_elb" {
  name        = "pipeline-vpc-elb"
  description = "Allow traffic to the VPC"
  vpc_id      = aws_vpc.pipeline_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "pipeline-vpc-elb"
    PROJECTTYPE = "pipeline"
  }
}

# Instance ELB
resource "aws_elb" "pipeline_elb" {
  name            = "pipeline-elb"
  security_groups = [aws_security_group.pipeline_elb_sg.id]
  subnets = [
    aws_subnet.pipeline_public_us_east_1a.id,
    aws_subnet.pipeline_public_us_east_1b.id
  ]
  cross_zone_load_balancing = true
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    target              = "HTTP:80/"
  }
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = "80"
    instance_protocol = "http"
  }
  #   instances = aws_instance.companyx_student_linux["${count.index + 1}"].id
  tags = {
    Name        = "Pipeline Elb"
    PROJECTTYPE = "pipeline"
  }
}

output "pipeline-elb-dns" {
  value = aws_elb.pipeline_elb.dns_name
}
