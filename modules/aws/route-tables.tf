# Route Table
resource "aws_route_table" "pipeline_route_table" {
  vpc_id = aws_vpc.pipeline_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.pipeline_igw.id
  }

  tags = {
    Name        = "pipeline-route-table"
    PROJECTTYPE = "pipeline"
  }
}

# Route Table Assocation
resource "aws_route_table_association" "pipeline_us_east_1a_public" {
  subnet_id      = aws_subnet.pipeline_public_us_east_1a.id
  route_table_id = aws_route_table.pipeline_route_table.id
}

resource "aws_route_table_association" "pipeline_us_east_1b_public" {
  subnet_id      = aws_subnet.pipeline_public_us_east_1b.id
  route_table_id = aws_route_table.pipeline_route_table.id
}