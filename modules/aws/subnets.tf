# Subnet 1
resource "aws_subnet" "pipeline_public_us_east_1a" {
  vpc_id            = aws_vpc.pipeline_vpc.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name        = "pipeline subnet us-east-1a"
    PROJECTTYPE = "pipeline"
  }
}

# Subnet 2
resource "aws_subnet" "pipeline_public_us_east_1b" {
  vpc_id            = aws_vpc.pipeline_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name        = "pipeline subnet us-east-1b"
    PROJECTTYPE = "pipeline"
  }
}