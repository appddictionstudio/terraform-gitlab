# VPC
resource "aws_vpc" "pipeline_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name        = "pipline-vpc"
    PROJECTTYPE = "pipeline"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "pipeline_igw" {
  vpc_id = aws_vpc.pipeline_vpc.id

  tags = {
    Name        = "companyx-igw"
    PROJECTTYPE = "pipeline"
  }
}