sudo yum update -y
sudo yum-config-manager --enable -y epel
sudo yum install git -y
sudo amazon-linux-extras install -y docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo git clone https://gitlab.com/appddictionstudio/gitlab-docker-compose.git

sudo docker volume create gitlab-data
sudo docker volume create gitlab-config
sudo docker volume create gitlab-logs
sudo docker volume create gitlab-runner-config

cd gitlab-docker-compose
pwd
docker-compose up -d