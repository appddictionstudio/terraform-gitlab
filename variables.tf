# Defining the Region for the S3 Bucket
variable "region" {
  type        = string
  default     = "us-east-1"
  description = "AWS Region"
}

# Instructor Information for IaC on AWS
variable "vault_password" {
  type        = string
  default     = "World2k!"
  description = "Instructor Password"
}

variable "vault_username" {
  type        = string
  default     = "appddiction1"
  description = "Instructor Username"
}