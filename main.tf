# AWS Module
module "aws" {
  source = ".//modules/aws"
}

# Configure Terraform to push production state to an S3 bucket. 
terraform {
  backend "s3" {
    bucket = "devsecops-bc-state"
    # Change your State file name
    key = "pipeline-terraform.state"
    # Change the location of the dynamoDB Table
    dynamodb_table = "pipeline-terraform-lock"
    region         = "us-east-1"
    # Change if needed if your profile is different. 
    # aws configure list -> To view your current profile
    # your profile list is in your user directory .aws credentials
    profile                 = "corp-appddiction1"
    shared_credentials_file = "~/.aws/credentials"
  }
}